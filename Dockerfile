# --------------------------------------------------------------
# Base Container
# ---------------------------------------------------------------
FROM alpine:3.13 as base
LABEL maintainer="9926281-shubham-gaur@users.noreply.gitlab.com"
RUN apk add --no-cache libressl-dev sqlite-dev tcl-dev zlib-dev curl alpine-sdk
### If you want to build "release", change the next line accordingly.
#ENV FOSSIL_INSTALL_VERSION trunk
ENV FOSSIL_INSTALL_VERSION version-2.22
RUN wget "https://www.fossil-scm.org/index.html/tarball/fossil-src.tar.gz" && tar -xvf fossil-src.tar.gz
WORKDIR /fossil-src
RUN ./configure --disable-fusefs --json --with-th1-docs --with-th1-hooks --with-tcl=1
RUN make && \
    strip fossil && \
    chmod a+rx fossil
RUN rm -vrf /var/cache/apk/*

# ---------------------------------------------------------------
# Main Container
# ---------------------------------------------------------------
FROM alpine:3.13
RUN apk add --no-cache libressl tcl
COPY --from=base /fossil-src/fossil /usr/bin/
RUN mkdir -p /opt/fossil
RUN rm -vrf /var/cache/apk/*
ENV HOME /opt/fossil
WORKDIR /opt/fossil
EXPOSE 8080
CMD ["/bin/sh", "-c", "/usr/bin/fossil server --user admin --create repository.fossil 2>&1"]

