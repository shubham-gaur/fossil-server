# Fossil Server

## Docker Run

1. Host a server container mounting the local path as a volume mount point.
1. Expose a system port to access the server

### Prerequisites

1. Create a hosting mount point `mkdir -p $HOME/local-host-dir`.
1. Treat this mount point as BACKUP point.

### Run

```shell
# <CONTAINER_NAME>: Application name
docker run --rm -d -p <SYSTEM_PORT>:8080 -v <BACKUP>:/opt/fossil --name test registry.gitlab.com/shubham-gaur/fossil-server:latest
```

### Get Credentials

* `docker logs -f <CONTAINER_NAME>`

## Fossil Clone

```shell
fossil clone http://<FOSSIL_USER>:<PASSWORD>@<FOSSIL_SERVER> repository.fossil
```

## Git Export

* `fossil git export git-mirrors -R ./fossil-server/fossil-server.fossil --autopush https://<username>:<password>@github.com/<username>/<repo>.git`

## Backup

```shell
# Create a cron job to backup automatically
# containing the following task

DATE=`date +%b-%m-%y`
TIME=`date +%H:%M`
echo $DATE $TIME

REPO=test
cp ${HOME}/fossils/${REPO}/fossil/${REPO}.fossil \
   ${HOME}/fossils/${REPO}/fossil/${REPO}_${DATE}~${TIME}.fossil
SRCFILE=${HOME}/fossils/${REPO}/fossil/${REPO}_${DATE}~${TIME}.fossil
DESFILE=$HOME/OneDrive/Projects/fossils/${REPO}_${DATE}~${TIME}.fossil
rm -f ${DESFILE}~*.fossil
cp ${SRCFILE}  ${DESFILE}
rm ${SRCFILE}
```

## Container Registry

* Latest image can be pulled using [`docker pull registry.gitlab.com/shubham-gaur/fossil-server:latest`]
* Other versions are available at container registry.

### Released

* registry.gitlab.com/shubham-gaur/fossil-server:v2.22
